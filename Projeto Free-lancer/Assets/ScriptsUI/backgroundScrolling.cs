using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Josemilson
{



    public class backgroundScrolling : MonoBehaviour
    {
        public MeshRenderer mr;
        public float velocidade;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            mr.material.mainTextureOffset += new Vector2(velocidade * Time.deltaTime, 0);
        }
    }
}