using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Josemilson
{


    public class FluxogramaAnim : MonoBehaviour
    {
        [Header("Animators")]
        [SerializeField] private Animator Equipe;

        [Header("GameObjects")]
        [SerializeField] private GameObject equipe;
        [SerializeField] private GameObject menu;
        [SerializeField] private GameObject play;


        [Header("time")]
        [SerializeField] private float tmToStopEquipe;
        [SerializeField] private float tmToOpenMenu;
        [SerializeField] private float tmToOpenPlay;

        private void Start()
        {
            StartCoroutine(ApagerEquipe());
        }

        IEnumerator ApagerEquipe()
        {
            yield return new WaitForSeconds(tmToStopEquipe);
            Equipe.Play("EquipeFadeOut");
            StartCoroutine(AbrirMenu());
        }

        IEnumerator AbrirMenu()
        {
            yield return new WaitForSeconds(tmToOpenMenu);
            menu.SetActive(true);
            StartCoroutine(AbrirPlay());
        }

        IEnumerator AbrirPlay()
        {
            yield return new WaitForSeconds(tmToOpenPlay);
            play.SetActive(true);
        }
    }
}