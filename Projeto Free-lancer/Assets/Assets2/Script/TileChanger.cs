using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileChanger : MonoBehaviour
{
    public Sprite spriteDesativado;
    public Sprite spriteAtivado;
    public bool active;

    private void Update()
    {
        if(active == true)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = spriteAtivado;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = spriteDesativado;
        }
    }

}
