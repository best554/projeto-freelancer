using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMove : MonoBehaviour
{
    bool isMoving;
    Vector3 origPost, targetPos, initPosit;
    float timeToMove = 0.3f;
    RaycastHit2D hit;
    public GameObject[] Tiles;
    public int total;
    public bool venceu;
    public GameObject chegada;

    private void Start()
    {
        total = 0;
        Tiles = GameObject.FindGameObjectsWithTag("Tile");
        initPosit = transform.position;
        chegada.SetActive(false);
    }

    void Update()
    {
        MoveKeys();
    }
    public void MoveKeys()
    {
        if (Input.GetKey(KeyCode.S) && !isMoving)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.down, 0.9f);
            if (hit.collider == null)
            {
                StartCoroutine(MovePlayer(Vector3.down));
            }
        }
        if (Input.GetKey(KeyCode.W) && !isMoving)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.up, 0.9f);
            if (hit.collider == null)
            {
                StartCoroutine(MovePlayer(Vector3.up));
            }
        }
        if (Input.GetKey(KeyCode.A) && !isMoving)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.left, 0.9f);
            if (hit.collider == null)
            {
                StartCoroutine(MovePlayer(Vector3.left));
            }
        }
        if (Input.GetKey(KeyCode.D) && !isMoving)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.right, 0.9f);
            if (hit.collider == null)
            {
                StartCoroutine(MovePlayer(Vector3.right));
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Tile"))
        {
            if(collision.GetComponent<TileChanger>().active == false)
            {
                collision.GetComponent<TileChanger>().active = true;
                total++;
            }
            else if(venceu == false)
            {
                foreach (GameObject i in Tiles)
                {
                    i.GetComponent<TileChanger>().active = false;
                    total = 0;
                }
            }
            if(total >= Tiles.Length)
            {
                venceu = true;
                chegada.SetActive(true);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Teleporte"))
        {
            transform.position = initPosit;
        }
    }

    public IEnumerator MovePlayer(Vector3 direction)
    {
        isMoving = true;
        float elapsedTime = 0;
        origPost = transform.position;
        targetPos = origPost + direction;
        
        while (elapsedTime < timeToMove)
        {
            transform.position = Vector3.Lerp(origPost, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.position = targetPos;
        isMoving = false;
    }
}
